%define apacheconfdir %{_sysconfdir}/httpd/conf.d
# this path is hardcoded
%define cblibdir %{_libdir}/policyd-2.0

%define version 2.0.14
%define release 1
%define tarver v%{version}

Summary: Postfix Policy Daemon
Name: cluebringer
Version: %{version}
Release: %{release}
License: GPLv2
Group: System/Daemons
URL: http://www.policyd.org
Source0: http://downloads.sourceforge.net/policyd/%{name}-%{tarver}.tar.bz2

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
BuildArch: noarch

Provides: cbpolicyd

Provides: policyd = %{version}
Obsoletes: policyd

Requires: perl(Net::Server), perl(Config::IniFiles), perl(Cache::FastMmap), httpd


%description
Policyd v2 (codenamed "cluebringer") is a multi-platform policy server
for popular MTAs. This policy daemon is designed mostly for large
scale mail hosting environments. The main goal is to implement as many
spam combating and email compliance features as possible while at the
same time maintaining the portability, stability and performance
required for mission critical email hosting of today. Most of the
ideas and methods implemented in Policyd v2 stem from Policyd v1
as well as the authors' long time involvement in large scale mail
hosting industry.


%prep
%setup -q -n %{name}-%{tarver}

# hack to prevent rpmbuild from automatically detecting "requirements" that
# aren't actually external requirements.  See https://fedoraproject.org/wiki/Packaging/Perl#In_.25prep_.28preferred.29
cat << EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* | sed -e '/perl(cbp::/d'
EOF

%define __perl_requires %{_builddir}/%{name}-%{tarver}/%{name}-req
chmod +x %{__perl_requires}


%build
cd database
for db_type in mysql4 mysql pgsql sqlite; do
	./convert-tsql ${db_type} core.tsql > policyd.${db_type}.sql
	for file in `find . -name \*.tsql -and -not -name core.tsql`; do
		./convert-tsql ${db_type} ${file}
	done >> policyd.${db_type}.sql
	cd whitelists
		./parse-checkhelo-whitelist >> policyd.${db_type}.sql
		./parse-greylisting-whitelist >> policyd.${db_type}.sql
	cd ..
done


%install
rm -rf $RPM_BUILD_ROOT


# cbpolicyd
mkdir -p $RPM_BUILD_ROOT%{cblibdir}
mkdir -p $RPM_BUILD_ROOT%{_sbindir}
mkdir -p $RPM_BUILD_ROOT%{_initrddir}
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/policyd
cp -R cbp $RPM_BUILD_ROOT%{cblibdir}
install -m 755 cbpolicyd cbpadmin database/convert-tsql $RPM_BUILD_ROOT%{_sbindir}
install -m 644 cluebringer.conf $RPM_BUILD_ROOT%{_sysconfdir}/policyd/cluebringer.conf
install -m 755 contrib/initscripts/Fedora/cbpolicyd $RPM_BUILD_ROOT%{_initrddir}

# Webui
mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{name}/webui
mkdir -p $RPM_BUILD_ROOT%{apacheconfdir}
cp -R webui/* $RPM_BUILD_ROOT%{_datadir}/%{name}/webui/
install -m 644 contrib/httpd/cluebringer-httpd.conf $RPM_BUILD_ROOT%{apacheconfdir}/cluebringer.conf
# Move config into /etc
mv $RPM_BUILD_ROOT%{_datadir}/%{name}/webui/includes/config.php $RPM_BUILD_ROOT%{_sysconfdir}/policyd/webui.conf
ln -s %{_sysconfdir}/policyd/webui.conf $RPM_BUILD_ROOT%{_datadir}/%{name}/webui/includes/config.php
chmod 0640 $RPM_BUILD_ROOT%{_sysconfdir}/policyd/webui.conf

# Docdir
mkdir -p $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/contrib
mkdir -p $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/database
install -m 644 AUTHORS INSTALL LICENSE TODO WISHLIST ChangeLog $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}
cp -R contrib $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/contrib/amavisd-new
install -m 644 database/*.sql $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/database


%post
/sbin/chkconfig --add cbpolicyd


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc %{_docdir}/%{name}-%{version}
%{cblibdir}/
%{_sbindir}/cbpolicyd
%{_sbindir}/cbpadmin
%{_sbindir}/convert-tsql
%{_initrddir}/cbpolicyd

%dir %{_datadir}/%{name}
%attr(-,root,apache) %{_datadir}/%{name}/webui/

%dir %{_sysconfdir}/policyd
%config(noreplace) %{_sysconfdir}/policyd/cluebringer.conf

%attr(-,root,apache) %config(noreplace) %{_sysconfdir}/policyd/webui.conf

%config(noreplace) %{apacheconfdir}/cluebringer.conf


%changelog

* Sun Nov 11 2012 Nigel Kukard <nkukard@lbsd.net>
Specfile should now get changelog automagically
Change-Id: I6daa22485f68e97e9b62b74658fd9431eae96d29

* Sun Nov 11 2012 Nigel Kukard <nkukard@lbsd.net>
Bumped 2.0.13
Change-Id: I16232759fc672c52ddd1a422dc8c2f19ba0376cc

* Wed Aug 29 2012 Nigel Kukard <nkukard@lbsd.net>
Merged in pipelining code from v2.1 branch
Change-Id: I6a1aea1a285ba5de9794808b8a1f1c6802f9a0fb

* Thu Jun 21 2012 Robert Anderson <randerson@lbsd.net>
Merge previous commit Fixed date
Change-Id: Ia112e388235c8f51226d20272cce32021e850730
Signed-off-by: Robert Anderson <randerson@lbsd.net>

* Fri Jun 01 2012 Nigel Kukard <nkukard@lbsd.net>
Add support for matching sender <> , use  @  as a policy member

* Fri Jun 01 2012 Nigel Kukard <nkukard@lbsd.net>
Added more debugging

* Tue May 15 2012 Nigel Kukard <nkukard@lbsd.net>
Added note on upgrading DB

* Thu May 10 2012 Nigel Kukard <nkukard@lbsd.net>
Bumped to v2.0.12

* Mon Feb 27 2012 Nigel Kukard <nkukard@lbsd.net>
Fixed name of changelog file

* Sun Jul 31 2011 Nigel Kukard <nkukard@lbsd.net>
Merge branch 'v2.0.x' of git.devlabs.linuxassist.net:policyd into v2.0.x

* Sun Jul 31 2011 Nigel Kukard <nkukard@lbsd.net>
Fixed splitting up of cidr_allow & cidr_deny - Thanks Ben Heilman ref: http://devlabs.linuxassist.net/issues/91

* Thu Jul 07 2011 Nigel Kukard <nkukard@lbsd.net>
Added patch by randerson to support MySQL 5.5

* Fri May 06 2011 Robert Anderson <randerson@lbsd.net>
Improved error handling for cleanup
Return -1 as used in the rest of policyd cleanup instead of carrying on after an error

* Tue May 03 2011 Robert Anderson <randerson@lbsd.net>
Use size values as kbyte instead of byte
Convert bytes to kbytes on a new request
Updated UPGRADING file with details on how to modify the database to accommodate the changes to policyd

NOTE: This commit will change the behaviour of message size counters used in policyd!

The following changes will need to be made to your database:

UPDATE quotas_limits, quotas_tracking
               SET quotas_limits.CounterLimit = ceil(quotas_limits.CounterLimit / 1024),
               quotas_tracking.Counter = ceil(quotas_tracking.Counter / 1024)
               WHERE quotas_tracking.QuotasLimitsID = quotas_limits.ID
               AND quotas_limits.Type = "MessageCumulativeSize";

UPDATE session_tracking        SET Size = ceil(Size / 1024);

* Thu Apr 21 2011 Nigel Kukard <nkukard@lbsd.net>
Version bump

* Sat Mar 26 2011 Nigel Kukard <nkukard@lbsd.net>
Small code cleanup

* Sat Mar 26 2011 Robert Anderson <randerson@lbsd.net>
Fixed email address matching when sender is empty
Sender may be null and shouldn't try to match against email specification

* Tue Dec 21 2010 Nigel Kukard <nkukard@lbsd.net>
-> Fixed issues in logs regarding to the quota usage * Fixed issue with quota usage increasing sporadically - Robert Anderson <randerson@lbsd.net>

* Sun Sep 19 2010 Nigel Kukard <nkukard@lbsd.net>
-> Allow matching of wildcards in email address specifications - Robert Anderson <randerson@lbsd.net>

* Mon Sep 06 2010 Nigel Kukard <nkukard@lbsd.net>
-> Added lib64 to v2.0.x aswell

* Thu Aug 26 2010 Nigel Kukard <nkukard@lbsd.net>
-> Race condition fix when updating quota values - Robert Anderson <randerson@lbsd.net>

* Mon May 17 2010 Nigel Kukard <nkukard@lbsd.net>
-> Fix cleanup of helo/ehlo data - Robert Anderson <randerson@lbsd.net>
Thanks Tomoyuki Murakami



* Wed Apr 14 2010 Nigel Kukard <nkukard@lbsd.net>
-> Fixed display of errors when there is a configuration issue

* Thu Apr 01 2010 Nigel Kukard <nkukard@lbsd.net>
-> Treat <> properly in tracking tables - Closes bug #16

* Mon Mar 15 2010 Nigel Kukard <nkukard@lbsd.net>
-> Fixed case issue with some database column names as this could cause some queries to fail

* Thu Dec 17 2009 Nigel Kukard <nkukard@lbsd.net>
-> Bumped to 2.0.11a

* Thu Dec 17 2009 Nigel Kukard <nkukard@lbsd.net>
-> Converted  template SQL statements into strings, bug crept in with a code sync to trunk

* Mon Dec 14 2009 Nigel Kukard <nkukard@lbsd.net>
-> Bumped version to 2.0.10a

* Tue Nov 24 2009 Nigel Kukard <nkukard@lbsd.net>
-> If we encounter an invalid IP in the greylisting whitelist, throw a warning not a hard error - Robert Anderson <randerson@lbsd.net>

* Tue Nov 24 2009 Nigel Kukard <nkukard@lbsd.net>
-> Cleaned up INSTALL doc and config file

* Mon Nov 16 2009 Nigel Kukard <nkukard@lbsd.net>
-> Better handling of CIDR's in parseCIDR() - Rober Anderson <randerson@lbsd.net>

* Tue Nov 10 2009 Nigel Kukard <nkukard@lbsd.net>
-> Protocol constant fix

* Wed Oct 14 2009 Nigel Kukard <nkukard@lbsd.net>
-> Version bump to 2.0.9a

* Fri Aug 21 2009 Nigel Kukard <nkukard@lbsd.net>
-> Default to unix logging to be compatibile with most installations as native is not yet supported - v2.1.x defaults to native

* Fri Aug 14 2009 Nigel Kukard <nkukard@lbsd.net>
-> More flexible configuration of logging

* Thu Jul 16 2009 Nigel Kukard <nkukard@lbsd.net>
-> Sender IP is a CIDR mask, not a network mask

* Wed Jul 15 2009 Nigel Kukard <nkukard@lbsd.net>
-> Fixes for greylisting portion of web interface, which is slighlty broken. - by Tomoyuki Murakami
I have noticed that setting greylisting preferences from webui updates 
(or reset) things in-correctly. For instance, GreylistUnAuthValidity 
cannot set any seconds. AWL After Percentage always reset to nothing 
(not NULL) and, so on.

Here is a patch to quick fix.




* Sun Jul 05 2009 Nigel Kukard <nkukard@lbsd.net>
-> Pulled in r472 from trunk, indentation fixup

* Mon Jun 29 2009 Nigel Kukard <nkukard@lbsd.net>
-> Fixed logging issues when protocol attributes are invalid

* Mon Jun 22 2009 Nigel Kukard <nkukard@lbsd.net>
-> Allow DBDo() to take parameters, this fixes bugs with all queries utilizing ?'s as placeholders - Thanks Peter Kiem

* Mon Jun 22 2009 Nigel Kukard <nkukard@lbsd.net>
-> Use Timestamp and not UnixTimestamp

* Sun Jun 21 2009 Nigel Kukard <nkukard@lbsd.net>
-> Fixed tempfail inversion - Closes bug #14

* Fri Jun 19 2009 Nigel Kukard <nkukard@lbsd.net>
-> Remove more table prefixes that crept in

* Fri Jun 19 2009 Nigel Kukard <nkukard@lbsd.net>
-> Table prefixes are not supported in v2.0.x yet

* Mon Jun 01 2009 Nigel Kukard <nkukard@lbsd.net>
-> Added support for OK in access control module

* Mon Jun 01 2009 Nigel Kukard <nkukard@lbsd.net>
-> Fixed bug when handling .something.example.com style reverse dns names

* Wed May 20 2009 Nigel Kukard <nkukard@lbsd.net>
-> Remember to closeCuror() before we start doing embedded queries, some versions of PHP don't like this - Thanks Eugene Krapivin

* Sun May 17 2009 Nigel Kukard <nkukard@lbsd.net>
-> Fixed AUTHORS

* Sat May 09 2009 Nigel Kukard <nkukard@lbsd.net>
-> Fixed logging lines being displayed when they were disabled

* Sat May 09 2009 Nigel Kukard <nkukard@lbsd.net>
-> Small copyright fixes

* Fri May 08 2009 Nigel Kukard <nkukard@lbsd.net>
-> Bump svn branch to 2.0.8a

* Tue May 05 2009 Nigel Kukard <nkukard@lbsd.net>
-> Copyright update

* Wed Apr 08 2009 Nigel Kukard <nkukard@lbsd.net>
-> Fixed bug where getPolicy was being called in the amavisd-policyd module with incorrect parameters. This was triggered when recipeits changed because of aliases or similar and the alias is not found in the session data stored during the Postfix RCPT checks.

* Sun Mar 29 2009 Nigel Kukard <nkukard@lbsd.net>
-> Fixed spurious "REJECT" in CheckHelo module

* Sat Mar 28 2009 Nigel Kukard <nkukard@lbsd.net>
-> Install cbpolicyd in sbin and cbpadmin in bin

* Sat Mar 28 2009 Nigel Kukard <nkukard@lbsd.net>
-> Fixed Amavis support, the last Received: header was being accessed in the wrong way

* Tue Mar 24 2009 Nigel Kukard <nkukard@lbsd.net>
-> No feedback from testers, assuming this patch fixes the greylisting race condition

* Sat Feb 28 2009 Nigel Kukard <nkukard@lbsd.net>
-> Add support for RFC3848 headers in amavis plugin

* Wed Feb 18 2009 Nigel Kukard <nkukard@lbsd.net>
-> Made old and new Config::IniFiles work

* Sat Feb 14 2009 Nigel Kukard <nkukard@lbsd.net>
-> Fixed up Config::IniFiles usage, this fixes breakage introduced with functionality changes in newer versions

* Mon Feb 09 2009 Nigel Kukard <nkukard@lbsd.net>
-> Fixed Postfix quota being overwritten

* Wed Jan 07 2009 Nigel Kukard <nkukard@lbsd.net>
-> Fixed default naming of the cluebringer database in the webui config to be inline with the other config file * Added note that the Postfix support in the webui is unsupported

* Tue Jan 06 2009 Nigel Kukard <nkukard@lbsd.net>
-> Bump branch to 2.0.7a

* Tue Jan 06 2009 Nigel Kukard <nkukard@lbsd.net>
-> Commit of resources required to build a .rpm

* Fri Jan 02 2009 Nigel Kukard <nkukard@lbsd.net>
-> Fixed bug in quotas where non-existant counters were being used

* Tue Nov 11 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed the rest of the options in greylisting not working with inherit option set

* Tue Nov 11 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed same inherit bug for percentages

* Tue Nov 11 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed changing of use autowhitelist/blacklist to work properly with changing to "Inherit"

* Tue Nov 11 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed disabling of greylisting, it showed up always as inherited and not disabled

* Mon Nov 10 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added requirement of Mail::SPF for SPF support

* Mon Nov 10 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added requirement for database server * Added requirement for PHP v5

* Mon Nov 10 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed bug where non-SASL authenticated email was being matched by the $* specification

* Thu Oct 30 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed bug where the last quota limit was reported as the one generating a verdict regardless of which one before it actually did

* Thu Oct 30 2008 Nigel Kukard <nkukard@lbsd.net>
-> Sucked in r369 from trunk, reversion of r327 - We already output logs regarding the verdict, no use doing it again

* Fri Oct 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fix error beign displayed when policy priority is 0

* Fri Oct 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Initialize recipient data to an empty hash, we may not even get any recipient data and we don't want this to be undefined

* Fri Oct 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Ignore recipient data in the DB if its null

* Fri Oct 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Make the CIDR value for IP specifications optional, an IP without it now defaults to /32

* Wed Oct 01 2008 Nigel Kukard <nkukard@lbsd.net>
-> Bumped version to 2.0.6a

* Tue Sep 30 2008 Nigel Kukard <nkukard@lbsd.net>
Pulled in r350 from trunk -> Updated links in pages to new website urls

* Tue Sep 30 2008 Nigel Kukard <nkukard@lbsd.net>
Pulled in r356 from trunk -> Clarified that policy priorities are prcessed in an ascending fashion, 0 being highest priority (first), 100 being the lowest (last).

* Sun Sep 28 2008 Nigel Kukard <nkukard@lbsd.net>
Pulled in r343 from trunk -> Better loadable module handling for cbpadmin

* Sun Sep 28 2008 Nigel Kukard <nkukard@lbsd.net>
-> Check the return codes from removing database entries more carefully

* Sun Sep 28 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed error message when deleting policies

* Sun Sep 28 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed module logging in main cbpolicyd server

* Sat Sep 27 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed logging when Quotas which are not matched

* Sat Sep 27 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added comment with mysql DB config in to amavisd-policyd integration module

* Sat Sep 27 2008 Nigel Kukard <nkukard@lbsd.net>
Pulled in r332 from trunk -> Removed stray file

* Sat Sep 27 2008 Nigel Kukard <nkukard@lbsd.net>
Pulled in r327 from trunk -> Fixed logging of verdict in quotas module

* Sat Sep 27 2008 Nigel Kukard <nkukard@lbsd.net>
-> no_quota should read no_verdict in Quotas module

* Fri Sep 26 2008 Nigel Kukard <nkukard@lbsd.net>
Pulled in r315 - Quotas: -> Fixed type on 'size' * Fixed typo in debug message * Fixed bug where end-of-data state was not being processed

* Fri Sep 26 2008 Nigel Kukard <nkukard@lbsd.net>
Pulled in r314 from trunk - -> Removed stray files

* Fri Sep 26 2008 Nigel Kukard <nkukard@lbsd.net>
Pulled in r316 from trunk - -> Fixed stray comma in Quotas module

* Sat Sep 20 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed bug in Quotas module where if all limits were disabled a temporary 450 result was generated - Thanks J. Duggan

* Thu Sep 11 2008 Nigel Kukard <nkukard@lbsd.net>
-> Add more debugging info to amavisd module so we can catch the received line if its not parsable

* Tue Sep 09 2008 Nigel Kukard <nkukard@lbsd.net>
-> Backported database portability fixes from trunk

* Tue Sep 09 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed use of short PHP tags <?  with  <?php

* Tue Sep 09 2008 Nigel Kukard <nkukard@lbsd.net>
-> Announce the version of policyd we're using when starting up

* Sat Sep 06 2008 Nigel Kukard <nkukard@lbsd.net>
-> Backported support for reverse DNS policies

* Fri Sep 05 2008 Nigel Kukard <nkukard@lbsd.net>
-> Ported commit 303 to stable v2.0.x

* Sat Aug 30 2008 Nigel Kukard <nkukard@lbsd.net>
-> Backported SASL username opiton in webui

* Thu Aug 21 2008 Nigel Kukard <nkukard@lbsd.net>
-> Version bump to 2.0.5a

* Wed Aug 20 2008 Nigel Kukard <nkukard@lbsd.net>
-> Merged in commits 290-294 from trunk, fixes bug #9

* Tue Aug 19 2008 Nigel Kukard <nkukard@lbsd.net>
-> Version bump to 2.0.4a

* Mon Aug 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed spelling mistakes & typos

* Mon Aug 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Better policy debugging * Various bug fixes and better handling of folding groups into policy sources and destinations

* Mon Aug 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed group negation in destination aswell

* Mon Aug 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed group negation - Big thanks to Johannes Russek for tracking this down

* Mon Aug 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Bumped SVN versions

* Mon Aug 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Forked off v2.0.x maintenance branch

* Mon Aug 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added special case $- for matching no SASL username - Thanks Jay

* Mon Aug 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Match SASL user first, so its not mistaken for a email address - Thanks Jay

* Thu Jul 31 2008 Nigel Kukard <nkukard@lbsd.net>
-> Must implement milter support at some stage

* Wed Jul 30 2008 Nigel Kukard <nkukard@lbsd.net>
-> Updated requirements

* Tue Jul 29 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added another requirement Cache::FastMmap

* Tue Jul 29 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added Config::IniFiles to Requirements list

* Tue Jul 29 2008 Nigel Kukard <nkukard@lbsd.net>
-> Updated todo

* Sun Jul 27 2008 Nigel Kukard <nkukard@lbsd.net>
-> Updated TODO

* Fri Jul 25 2008 Nigel Kukard <nkukard@lbsd.net>
-> Updated TODO, setup lists of features to come into 2.1

* Fri Jul 25 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added TODO for helo blacklist wildcard matching

* Fri Jul 25 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed DEFER option for quotas

* Thu Jul 24 2008 Nigel Kukard <nkukard@lbsd.net>
-> Updated TODO with whitelist & blacklist support

* Mon Jul 21 2008 Nigel Kukard <nkukard@lbsd.net>
-> Version bump to 2.0.2

* Fri Jul 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Updated todo

* Fri Jul 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Initialize protocol before processing requests

* Fri Jul 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added missing greylisting.tsql to install howto

* Tue Jul 15 2008 Nigel Kukard <nkukard@lbsd.net>
-> Add option to defer on quota exceed

* Wed Jul 09 2008 Nigel Kukard <nkukard@lbsd.net>
-> Updated TODO

* Sun Jul 06 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added policyd support patch for amavisd-new 2.6.1

* Sat Jul 05 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed stray '

* Thu Jul 03 2008 Nigel Kukard <nkukard@lbsd.net>
-> Few clarifications on installation

* Thu Jul 03 2008 Nigel Kukard <nkukard@lbsd.net>
-> When removing a Postfix email address, remove it from all distribution groups

* Thu Jul 03 2008 Nigel Kukard <nkukard@lbsd.net>
-> Order Postfix distribution group display

* Thu Jul 03 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fix for webui, when creating Postfix configuration don't default to eventhing being disabled - Thanks Arno

* Fri Jun 27 2008 Nigel Kukard <nkukard@lbsd.net>
-> Better support for newer versions of Cache-FastMmap

* Fri Jun 27 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed handling of Postfix policy delegation protocol

* Thu Jun 26 2008 Nigel Kukard <nkukard@lbsd.net>
-> Set table charset to latin1 for MySQL or we exceed the MySQL index length limitation

* Thu Jun 26 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed rogue ,'s in SQL * Added CREATE INDEX instead of INDEX() for better cross-database support * Added indexing back to sqlite

* Wed Jun 25 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed typo and better error message - Thanks Ghen

* Tue Jun 24 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed support for broken browsers

* Sun Jun 08 2008 Nigel Kukard <nkukard@lbsd.net>
-> Improve indexing of columns used during cleanup and queries

* Sun Jun 08 2008 Nigel Kukard <nkukard@lbsd.net>
-> Improve query speed when cleaning up

* Sun Jun 08 2008 Nigel Kukard <nkukard@lbsd.net>
-> Comitted patch from Geert to fix cbpadmin printing everything out to STDERR

* Tue May 13 2008 Nigel Kukard <nkukard@lbsd.net>
-> Make sure HELO is set, else we are trying to log undefined values

* Mon May 12 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed SQL syntax to be compatible with PostgreSQL

* Wed May 07 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added configurable return message result todo

* Wed May 07 2008 Nigel Kukard <nkukard@lbsd.net>
-> Updated TODO & WISHLIST

* Tue May 06 2008 Nigel Kukard <nkukard@lbsd.net>
-> Updated TODO

* Tue May 06 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed database timeout not working, thanks Geert * Removed duplicate default config for 'timeout'

* Wed Apr 30 2008 Nigel Kukard <nkukard@lbsd.net>
-> Allow use of "helo_name" in HTTP protocols

* Wed Apr 30 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed typo

* Wed Apr 30 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed IP CIDR's for local ranges, resolves #5 & #6

* Sat Apr 05 2008 Nigel Kukard <nkukard@lbsd.net>
-> Use VARCHAR(255) instead to TINYTEXT to be more portable - Thanks Andreas

* Wed Apr 02 2008 Nigel Kukard <nkukard@lbsd.net>
-> Quotas: We use big integers for timestamps not DATETIME - Thanks Andreas

* Wed Apr 02 2008 Nigel Kukard <nkukard@lbsd.net>
-> PostgreSQL requires 'PRIMARY KEY' to be specified for SERIAL types to achive the same as MySQL's SERIAL type - Thanks Andreas

* Mon Mar 31 2008 Nigel Kukard <nkukard@lbsd.net>
-> Whitelisting must be done by ech module

* Fri Mar 28 2008 Nigel Kukard <nkukard@lbsd.net>
-> Bigfish.com operates a large number of servers behind single IP's

* Wed Mar 26 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added support to display, #1

* Tue Mar 25 2008 Nigel Kukard <nkukard@lbsd.net>
-> Updated TODO

* Mon Mar 24 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed disabled flag not working with postfix mailboxes

* Mon Mar 24 2008 Nigel Kukard <nkukard@lbsd.net>
-> Encrypt mailbox passwords

* Fri Mar 21 2008 Nigel Kukard <nkukard@lbsd.net>
-> small JS fixup not to display tooltip for normal links

* Fri Mar 21 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added fancy tooltips to most of the config options to make it easier for first-timers to configure Policyd v2

* Thu Mar 20 2008 Nigel Kukard <nkukard@lbsd.net>
-> Close cursor on fetchObject

* Thu Mar 20 2008 Nigel Kukard <nkukard@lbsd.net>
-> Removed a debug line

* Thu Mar 20 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed bugin amavisd-change regarding list fields * Added Protocols to main features page

* Thu Mar 20 2008 Nigel Kukard <nkukard@lbsd.net>
-> Updated install document

* Thu Mar 20 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added library path to executables & amavisd module

* Wed Mar 19 2008 Nigel Kukard <nkukard@lbsd.net>
-> Check if result is defined, not the result of the function

* Wed Mar 19 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added support to tune server preforking mechanism

* Wed Mar 19 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed pretty serious bug, when one specified auto blacklisting percentage, if the count was reached it would always blacklist the host

* Wed Mar 19 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed typo in accesscontrol-add

* Wed Mar 19 2008 Nigel Kukard <nkukard@lbsd.net>
-> Remove ID column from session_tracking, requirement for this was removed yesterday

* Tue Mar 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Removed some debugging info which shouldn't be here

* Tue Mar 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added config option log_mail, this is where we log the mail logs to ... either "main" or "maillog" - main logs to cbpolicyd's log mechanism - maillog logs to syslog under mail/info

* Tue Mar 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> We don't use GreylistingID anymore, did we ever?

* Tue Mar 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed cidr_allow and cidr_deny. Comma, whitespace or semi-colon separated.

* Tue Mar 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed typo in debug messages, this is Bizanga, not Postfix

* Tue Mar 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added more protocol validation debugging

* Tue Mar 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Allow <> email address to be used as sender in protocols data validation functions

* Tue Mar 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Use count(*) instead of count(ID)

* Tue Mar 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed up debug message format a bit * Added protocol data validation

* Tue Mar 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Sanitized the HTTP response codes for Bizanga

* Tue Mar 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> close cursor when we done with it

* Tue Mar 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Store ProtocolTransport in session data

* Tue Mar 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed uninitialized errors while doing a cleanup * Fixed typo in tracking.pm

* Tue Mar 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added index to tracking table for those databases which support indexes * Removed requirement on ID column for tracking of smtp sessions

* Tue Mar 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Removed ID column from tracking table

* Tue Mar 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Removed requirement for ID auto-increment column

* Tue Mar 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Major schema change, remove the auto-incrementing ID column

* Tue Mar 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added support for database specific tracking key length * Added support for indexes, some databases don't like indexes?? (sqlite)

* Tue Mar 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added some error checking and cleaned up commandline usage

* Tue Mar 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added bypass_timeout, this delays the destruction of the child and database reconnection

* Tue Mar 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed database bypass mode, we were looking in the [server] section and not in the [database] section

* Tue Mar 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Removed stray debugging info

* Tue Mar 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Major schema change: - Removed Authenticated column - Added Tries column, this counts how many ties a greylisting entry is tried and fails - Added Count column, this counts how many mails pass through after being authenticated

* Tue Mar 18 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed some logging bugs

* Mon Mar 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed database connection issues with new BYPASS code

* Mon Mar 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added closing of cursor when we're done with it, this should solve some LAMP issues

* Mon Mar 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed multiple logging bugs * Standardized logging

* Mon Mar 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added database bypass_mode option, you can now tempfail or defer based on database failure

* Mon Mar 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Updated amavisd plugin to new framework

* Mon Mar 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed using non-existent logging mechanism

* Mon Mar 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed logging in tracking module, mostly relating to external plugins

* Mon Mar 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added config variable log_detail which you can use to specify extreme detail to show in debugging for a specific module/section of policyd

* Mon Mar 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed error... Use of uninitialized value in concatenation (.) or string at cbp/protocols/Bizanga.pm line 215. - Thanks Geert

* Mon Mar 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> More debugging info * Fixed bug where we were not getting a policy for HTTP protocols

* Mon Mar 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added whacks of debugging info

* Mon Mar 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Major change, renamed policy acls to policy members. This is less confusing. * Schema changed, renamed policy_acls to policy_members

* Mon Mar 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Better protocol module handling, don't panic and die if we cannot identify the first line straight away * Cosmetic fix in system.pm

* Mon Mar 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed bug with uninitalized SessionData * Fixed bug when HTTP protocols are not recognised (spelling mistake)

* Mon Mar 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Remove extra newline at end of HTTP protocol

* Mon Mar 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added some debugging to Postfix protocol to predict if it may be Postfix protocol or not

* Mon Mar 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Better Bizanga protocol matching

* Sun Mar 16 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added some extra debug to catch some potential bugs

* Sun Mar 16 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed returns in Greylisting, this should be the new framework

* Sun Mar 16 2008 Nigel Kukard <nkukard@lbsd.net>
-> XHML 1.0 Strict fixes

* Sun Mar 16 2008 Nigel Kukard <nkukard@lbsd.net>
-> Add <> to tracking of sessions

* Sun Mar 16 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added some debugging info to tracking.pm to try catch uninitialized data bug

* Sun Mar 16 2008 Nigel Kukard <nkukard@lbsd.net>
-> Skip SPF if we have no local explanation

* Sun Mar 16 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed typo in variable name

* Sun Mar 16 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed whitelisting in CheckHelo and Greylisting

* Sun Mar 16 2008 Nigel Kukard <nkukard@lbsd.net>
-> Add parsed client address to session data

* Sun Mar 16 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed some debugging

* Sun Mar 16 2008 Nigel Kukard <nkukard@lbsd.net>
-> Try 15 second timeout on resolving SPF, some queries took minutes due to SERVFAIL which generated very high load

* Sat Mar 15 2008 Nigel Kukard <nkukard@lbsd.net>
-> Bizanga PASS should be code 200 not 503

* Sat Mar 15 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed web interface with changes in schema over the past week

* Sat Mar 15 2008 Nigel Kukard <nkukard@lbsd.net>
-> Removed some debugging info

* Sat Mar 15 2008 Nigel Kukard <nkukard@lbsd.net>
-> Forgot to add protocol constants file

* Sat Mar 15 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed Bizanga response generation, perl seems to like this better

* Sat Mar 15 2008 Nigel Kukard <nkukard@lbsd.net>
-> Ported all modules over to new protocol-neutral framework

* Sat Mar 15 2008 Nigel Kukard <nkukard@lbsd.net>
-> Checkin of protocol updates * Renamed HTTP to Bizanga

* Sat Mar 15 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed IP arithmatic in policies.pm ... this looks better

* Sat Mar 15 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fix byte-order in translation of IP's to unsigned long integers and visa versa

* Fri Mar 14 2008 Nigel Kukard <nkukard@lbsd.net>
-> Differentiate tracking based on protocol transport

* Fri Mar 14 2008 Nigel Kukard <nkukard@lbsd.net>
-> Various cosmetic fixes * Changed _Policy to Policy in session data hash * Fixed a spelling mistake or two

* Fri Mar 14 2008 Nigel Kukard <nkukard@lbsd.net>
-> Parse HTTP GET into request hash & set protocol transport

* Fri Mar 14 2008 Nigel Kukard <nkukard@lbsd.net>
-> Set protocol transport for Postfix

* Thu Mar 13 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added protocol parser for HTTP requests

* Thu Mar 13 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added support for protocol plugins * Rewrote the network IO handling in cbpolicyd, no more use of alarms, rather use select()

* Thu Mar 13 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added postfix and HTTP protocol plugins

* Wed Mar 12 2008 Nigel Kukard <nkukard@lbsd.net>
-> Lowercase SASL username aswell

* Wed Mar 12 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed parseCIDR function, there was a typo breaking everyhing!

* Wed Mar 12 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed support for SenderIP: syntax in CheckHelo and Greylisting module

* Wed Mar 12 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed typo

* Wed Mar 12 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed up CheckHelo column Address, renamed it to Source as it supports various kinds of formats (or will in future)

* Wed Mar 12 2008 Nigel Kukard <nkukard@lbsd.net>
-> Skip over modules without checks defined

* Wed Mar 12 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed bugs regarding debugging and foreground mode

* Wed Mar 12 2008 Nigel Kukard <nkukard@lbsd.net>
-> Updated readme with core module priority

* Wed Mar 12 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added cleanup functionality

* Wed Mar 12 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added basic cleanup functionality, this should more than likely need to be refined based on the limits period

* Wed Mar 12 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added cleanup functionality

* Wed Mar 12 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added admin tool, the main purpose of this tool at the moment is to do the DB cleanup

* Wed Mar 12 2008 Nigel Kukard <nkukard@lbsd.net>
-> Removed redundant function

* Wed Mar 12 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added core module, this is for system functions used in a modular fashion (ie. the cleanup tool)

* Wed Mar 12 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added version module to be shared between commandline tools

* Wed Mar 12 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed background vs. foreground issues in cbpolicyd * Fixed defaults * Added  --fg  for forced foreground operation

* Wed Mar 12 2008 Nigel Kukard <nkukard@lbsd.net>
-> List of hosts for checkhelo and greylisting exceptions - Please feel free to post nominations to developers mailing list!

* Wed Mar 12 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed daemonization, cbpolicyd is now daemonized by default * Set a few config defaults in cbpolicyd

* Wed Mar 12 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed auto-whitelisting, it would of worked in the opposite way as expected * Cosmetic fixes

* Tue Mar 11 2008 Nigel Kukard <nkukard@lbsd.net>
-> Updated index page

* Tue Mar 11 2008 Nigel Kukard <nkukard@lbsd.net>
-> Honor the %% specified in the Greylisting rule properly, its %% of total triplets, not unauthenticated vs. authenticated

* Tue Mar 11 2008 Nigel Kukard <nkukard@lbsd.net>
-> Stop processing modules if we have a verdict

* Tue Mar 11 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed auto-whitelisting and auto-blacklisting * Updated TODO with a good greylisting idea from Xpoint

* Tue Mar 11 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed multiple SQL query errors * Some cosmetic fixes

* Tue Mar 11 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed problems when changing a checkhelo, "UNKNOWN" was displayed instead of "No"

* Tue Mar 11 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added TXT file with some module priorities

* Tue Mar 11 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added greylisting webui

* Tue Mar 11 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed label on tab at top of page

* Tue Mar 11 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed missing track by 'Policy' option * Cleaned up quotas deleting

* Tue Mar 11 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added greylisting sql tables

* Tue Mar 11 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added greylisting support * Updated config file example and cleaned it up a bit

* Tue Mar 11 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added defer support to cbpolicyd * Updated TODO

* Tue Mar 11 2008 Nigel Kukard <nkukard@lbsd.net>
-> Reduce address field to 64 characters instead of 255

* Tue Mar 11 2008 Nigel Kukard <nkukard@lbsd.net>
-> Various code cleanups * Added priority to each module

* Tue Mar 11 2008 Nigel Kukard <nkukard@lbsd.net>
-> Use parseCIDR * Various code cleanups * Added module priority

* Tue Mar 11 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added parsing of CIDR support, this code is repeated in numerous places

* Tue Mar 11 2008 Nigel Kukard <nkukard@lbsd.net>
-> Call getPolicy function with SASL information * Small cosmetic fixes in amavis plugin

* Tue Mar 11 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added SASL support to policy library * Fixed bug in parsing in policy rules regarding email address specifications * Some small regex cleanups

* Thu Mar 06 2008 Nigel Kukard <nkukard@lbsd.net>
-> Minor bugfixes and cosmetic changes to webui

* Wed Mar 05 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed various record removal issues

* Wed Mar 05 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added some checks in cbpolicyd for failed session data functions * Removed some debugging from tracking.pm

* Wed Mar 05 2008 Nigel Kukard <nkukard@lbsd.net>
-> Ported amavisd module to new framework

* Wed Mar 05 2008 Nigel Kukard <nkukard@lbsd.net>
-> Major changes to entire framework, this new framework allows tracking of email through policyd v2 and into a 3rd-party module

* Wed Mar 05 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed bug with disabled postfix aliases * Remvoed debug information from amavis-add

* Wed Mar 05 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added message tacking module

* Sat Mar 01 2008 Nigel Kukard <nkukard@lbsd.net>
-> Updated TODO * Fixed typo in CheckSPF module

* Sat Mar 01 2008 Nigel Kukard <nkukard@lbsd.net>
-> Make SPF reason for rejection include link and be more pretty

* Sat Mar 01 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added Amavis always_bcc option as Interception/BCC To

* Sat Mar 01 2008 Nigel Kukard <nkukard@lbsd.net>
-> Disable postfix integration by default

* Sat Mar 01 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added main.cf_snippet

* Sat Mar 01 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added removing of child table columns on some of the modules * Added displaying of SQL errors on error

* Sat Mar 01 2008 Nigel Kukard <nkukard@lbsd.net>
-> added some error checking to some of the webui

* Sat Mar 01 2008 Nigel Kukard <nkukard@lbsd.net>
-> Check if we must add a SPF header when the domain doesn't have a SPF record

* Sat Mar 01 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed referential integrity bug reported by xpoint on irc

* Sat Mar 01 2008 Nigel Kukard <nkukard@lbsd.net>
-> Catch invalid HELO's seeping through the cracks

* Sat Mar 01 2008 Nigel Kukard <nkukard@lbsd.net>
-> Use plain local_explanation and not Dumper()

* Sat Mar 01 2008 Nigel Kukard <nkukard@lbsd.net>
-> Use ; in rejections instead of , * Don't do SPF check on <>

* Sat Mar 01 2008 Nigel Kukard <nkukard@lbsd.net>
-> Use ; instead of , in the rejection messages

* Sat Mar 01 2008 Nigel Kukard <nkukard@lbsd.net>
-> Compare database values using text comparison instead of numeric * Added some debug info to SPF

* Sat Mar 01 2008 Nigel Kukard <nkukard@lbsd.net>
-> Removed comments at bottom of config file

* Sat Mar 01 2008 Nigel Kukard <nkukard@lbsd.net>
-> Updated install howto

* Sat Mar 01 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added short install howto

* Fri Feb 29 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added CheckSPF module to cbpolicyd

* Fri Feb 29 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added web interface for CheckSPF module

* Thu Feb 28 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added CheckHelo module to backend * Various small fixes to Quotas and AccessControl

* Thu Feb 28 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added HELO/EHLO checks to webui

* Sun Feb 24 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed configuration for postfix distribution lists to use new table naming scheme

* Sat Feb 23 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added contributed postfix config

* Sat Feb 23 2008 Nigel Kukard <nkukard@lbsd.net>
-> Finished support for optional postfix addon to web interface

* Sat Feb 16 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added postfix transport & mailbox creation support .... this will be more than likely split off into another project as it does not belong in cluebringer

* Thu Feb 07 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added patch to add policy changing to amavisd-new * Updated amavis custom module and removed most debugging

* Tue Feb 05 2008 Nigel Kukard <nkukard@lbsd.net>
-> Quite a few fixups to the amavisd plugin module, it still doesn't support merging of certain options (wip).

* Tue Feb 05 2008 Nigel Kukard <nkukard@lbsd.net>
-> This commit should add Amavisd-new integration into the web interface * Various fixups

* Sun Feb 03 2008 Nigel Kukard <nkukard@lbsd.net>
-> Changed amavis table to add _rules

* Mon Jan 28 2008 Nigel Kukard <nkukard@lbsd.net>
-> Preliminary contributed module for policyd => amavis integration - This will allow for all amavisd features to be available on a per policyd-policy basis, among other things just think about only scanning email for spam if its from a freemail provider, or even setting spam scores especially on freemail providers sending to commonly spammed email addies. The limits are endless.

* Mon Jan 28 2008 Nigel Kukard <nkukard@lbsd.net>
-> Removed DBFreeRes where its not required

* Mon Jan 28 2008 Nigel Kukard <nkukard@lbsd.net>
-> Removed DBFreeRes where its not needed

* Mon Jan 28 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added database schema for amavis integration

* Thu Jan 24 2008 Nigel Kukard <nkukard@lbsd.net>
-> We use PDO, not MDB2 now

* Thu Jan 24 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added customizable returns to quotas being exceeded

* Thu Jan 24 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed deleting of quotas, remove limits and limit tracking * Small code fixes & cleanups

* Wed Jan 23 2008 Nigel Kukard <nkukard@lbsd.net>
-> Avoid colum name clashing in mysql

* Wed Jan 23 2008 Nigel Kukard <nkukard@lbsd.net>
-> WebUI - Commit of the remainding of the quota support

* Wed Jan 23 2008 Nigel Kukard <nkukard@lbsd.net>
-> Various schema changes

* Wed Jan 23 2008 Nigel Kukard <nkukard@lbsd.net>
-> WebUI - Added support for Quotas - Added support for AccessControl - Lots of changes regarding compliance with the relevant specs

* Mon Jan 21 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added support for policies * Added support for policy acls * Added support for policy groups * Added support for policy group members

* Mon Jan 21 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added comment to policy acls

* Sat Jan 19 2008 Nigel Kukard <nkukard@lbsd.net>
-> WebUI: Adding of policies now work

* Sat Jan 19 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added webui framework

* Thu Jan 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added wishlist

* Thu Jan 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Quotas: Added support to track based on policy

* Thu Jan 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added logging to maillog

* Thu Jan 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed up the allowed configuration options * Added support to log to maillog

* Thu Jan 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added missing configuration options & some comments - Updated site with more verbose description

* Thu Jan 17 2008 Nigel Kukard <nkukard@lbsd.net>
-> Don't use #, use /*   */  in sql

* Wed Jan 16 2008 Nigel Kukard <nkukard@lbsd.net>
-> Version bump before snapshot

* Wed Jan 16 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added script to convert tsql files into mysql, pgsql and sqlite format

* Wed Jan 16 2008 Nigel Kukard <nkukard@lbsd.net>
-> Fixed mysql and pgsql conversion scripts - Thanks xpoint

* Wed Jan 16 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added quotas support

* Wed Jan 16 2008 Nigel Kukard <nkukard@lbsd.net>
-> cbpolicyd: Track recipients so we can see who's getting the message in EOM * database/core: Added SQL table for the recipient tracking

* Wed Jan 16 2008 Nigel Kukard <nkukard@lbsd.net>
-> Commented out access control example

* Wed Jan 16 2008 Nigel Kukard <nkukard@lbsd.net>
-> Removed debug info * Various cosmetic fixes

* Wed Jan 16 2008 Nigel Kukard <nkukard@lbsd.net>
-> Removed stray use

* Wed Jan 16 2008 Nigel Kukard <nkukard@lbsd.net>
-> Export IP arithmatic functions

* Fri Jan 11 2008 Nigel Kukard <nkukard@lbsd.net>
Policies: -> Remove debuggin info from policy engine * Push policies into an array hashed by policy
AccessControl:
-> Basic implementation




* Thu Jan 10 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added simple access control schema

* Thu Jan 10 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added simple db creation script

* Thu Jan 10 2008 Nigel Kukard <nkukard@lbsd.net>
-> Sanitized the default policies

* Thu Jan 10 2008 Nigel Kukard <nkukard@lbsd.net>
-> Checked in system module * Checked in policiesm odule * Checked in caching module

* Tue Jan 08 2008 Nigel Kukard <nkukard@lbsd.net>
-> Added config module * Removed modules.pm, don't need this anymore * Added DBI layers to support embedded transactions * Removed ltable.pm, we'll use direct DB access from now on * Updated config file

* Tue Jan 08 2008 Nigel Kukard <nkukard@lbsd.net>
-> Removed feature moduels for now * Added database schema for new policy support * Added schema for policy based quotas support

* Mon Jan 07 2008 Nigel Kukard <nkukard@lbsd.net>
-> Renamed schemas dir to database

* Mon Dec 17 2007 Nigel Kukard <nkukard@lbsd.net>
-> Commit of my work so far

* Sat Dec 08 2007 Nigel Kukard <nkukard@lbsd.net>
-> Added immediate requirements for contract sponsoring development

* Tue Jun 26 2007 Nigel Kukard <nkukard@lbsd.net>
-> Various changes & fixups

* Sun Jun 24 2007 Nigel Kukard <nkukard@lbsd.net>
-> Reworked databae handling * Reworked feature & database modules * Fixed up logging a bit

* Fri Jun 22 2007 Nigel Kukard <nkukard@lbsd.net>
-> If a database fails to initialize, don't use it * Addd getStatus() function to backend database and ltable layer

* Fri Jun 22 2007 Nigel Kukard <nkukard@lbsd.net>
-> More major changes

* Mon Jun 18 2007 Nigel Kukard <nkukard@lbsd.net>
-> Semi-working lookup abstraction layer * Added schmas/mysql/helo.sql schema

* Sun Jun 17 2007 Nigel Kukard <nkukard@lbsd.net>
-> Semi working database/table abstraction layer

* Sat Jun 16 2007 Nigel Kukard <nkukard@lbsd.net>
-> Preliminary work on database table support
[code is currently broken and will not run]



* Sat Jun 16 2007 Nigel Kukard <nkukard@lbsd.net>
-> Renamed features to feature

* Sat Jun 16 2007 Nigel Kukard <nkukard@lbsd.net>
-> Preliminary checkin, moved features into features directory

* Tue Jun 12 2007 Nigel Kukard <nkukard@lbsd.net>
-> Added TODO  for per recipient whitelists & blacklists

* Sun Jun 10 2007 Nigel Kukard <nkukard@lbsd.net>
-> Fixed up logging

* Sat Jun 09 2007 Nigel Kukard <nkukard@lbsd.net>
-> Better config support, preparing for some sort of abstraction the modules can use

* Sat Jun 09 2007 Nigel Kukard <nkukard@lbsd.net>
-> Added skeleton for helo based blacklisting/whitelisting ... etc

* Sat Jun 09 2007 Nigel Kukard <nkukard@lbsd.net>
-> Added reversing of sending server IP check * Reworked handling of resolution results of FQDN HELO check

* Sat Jun 09 2007 Nigel Kukard <nkukard@lbsd.net>
-> Check for valid DNS records when verifying HELO/EHLO

* Sat Jun 09 2007 Nigel Kukard <nkukard@lbsd.net>
-> Added DNS checking of HELO/EHLO

* Sat Jun 09 2007 Nigel Kukard <nkukard@lbsd.net>
-> Added FIXME's for helo fqdn check module * Possibly bypass HELO checks for sasl users

* Sat Jun 09 2007 Nigel Kukard <nkukard@lbsd.net>
-> Misc bugfixes * Working FQDN HELO checking

* Fri Jun 08 2007 Nigel Kukard <nkukard@lbsd.net>
-> Added time to greylisting

* Fri Jun 08 2007 Nigel Kukard <nkukard@lbsd.net>
-> Fixed typo in module result handling

* Fri Jun 08 2007 Nigel Kukard <nkukard@lbsd.net>
-> Fixed typo in greylisting module

* Fri Jun 08 2007 Nigel Kukard <nkukard@lbsd.net>
-> Added module logging function * Log what we see in greylisting * Lowercase sender & recipient

* Fri Jun 08 2007 Nigel Kukard <nkukard@lbsd.net>
-> Fixed up syslog levels * Log to syslog in main program, instead of STDOUT

* Fri Jun 08 2007 Nigel Kukard <nkukard@lbsd.net>
-> Added some stuff to work off of

* Fri Jun 08 2007 Nigel Kukard <nkukard@lbsd.net>
-> Added main svn structure

